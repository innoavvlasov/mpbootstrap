import { WorkflowAction } from './actions';
import { WorkflowStatus } from './constants';
import { Error, HistoryItem } from './response';

interface State {
  stateName?: string;
  flowName?: string;
  processId?: string;
  url?: string;
  isLoading: boolean;
  error?: Error;
  data?: Object;
  status?: WorkflowStatus;
  history?: HistoryItem[];
  bundleName?: string;
}

export const workflow = (
  state: State = { isLoading: true },
  action: WorkflowAction
) => {
  let { options, body, error } = action;
  switch (action.type) {
    case 'WF_INIT':
      return {
        ...state,
        isLoading: true,
        url: options.url,
        flowName: options.flowName
      };
    case 'WF_SUCCESS':
      return {
        ...state,
        isLoading:
          ['EXTERNAL_ENTER', 'EXTERNAL_RETURN'].indexOf(body.result) > -1,
        status: WorkflowStatus[body.result],
        stateName: body.state, 
        flowName: body.flow,
        processId: body.pid,
        url: body.url,
        data: body.output,
        history: body.history
      };
    case 'WF_ERROR':
      return {
        ...state,
        isLoading: false,
        error: error
      };
    case 'WF_SEND_COMMAND':
      return {
        ...state,
        isLoading: true
      };

    default:
      return state;
  }
};
